FROM mhart/alpine-node:base-6.2.1

ENV NODE_ENV production

EXPOSE 3000

WORKDIR /demo

COPY . /demo

RUN /demo/node_modules/webpack/bin/webpack.js --output-path static

CMD [ "node", "src/index.js" ]
