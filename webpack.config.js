'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

const resolve = path.resolve;
const env = process.env.NODE_ENV || 'development';
const isProd = env === 'production';

const config = {
  resolve: {
    extensions: ['', '.js', '.jsx'],
    root: resolve(process.cwd(), 'src')
  },
  entry: [ './src/main.js' ],
  output: {
    path: resolve(process.cwd(), 'build'),
    filename: 'index.js',
    publicPath: '/static/'
  },
  plugins: [
  ],
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      loaders: ['babel']
    }, {
      test: /\.css$/,
      // ExtractTextPlugin is breaking css hot loading, so only enable for prod
      loader: 'style!css!postcss'
    }, {
      test  : /\.(otf|eot|svg|ttf|woff)/,
      loader: 'url-loader?limit=12'
    }, {
      test   : /\.(gif|jpg|png)$/,
      loader : 'file-loader?name=./images/[name].[hash].[ext]'
    }]
  },
  postcss(webpack) {
    return [
      require('postcss-import')({ addDependencyTo: webpack }),
      require('postcss-url')(),
      require('postcss-cssnext')(),
      require('postcss-browser-reporter')(),
      require('postcss-reporter')()
    ];
  }
};

if (!isProd) {
  // eval-based sourcemaps cause breakpoints to be skipped on initial page load on Chrome.
  // https://github.com/webpack/webpack/issues/740
  /* config.devtool = 'cheap-module-eval-source-map'; */

  // Using 'cheap-module-source-map' fixes it but is slower to reload on hot loading
  // When the issue is fixed we can move back to 'cheap-eval-source-map'
  config.devtool = 'cheap-module-source-map';

  config.plugins.push(
      new webpack.HotModuleReplacementPlugin()
  );
}

if (isProd) {
  config.plugins.push(
      // ExtractTextPlugin is breaking css hot loading, so only enable for prod
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        },
        output: {
          comments: false
        },
        sourceMap: false
      }),
      new webpack.DefinePlugin({
        'process.env': { NODE_ENV: JSON.stringify(env) }
      })
  );
}

module.exports = config;
