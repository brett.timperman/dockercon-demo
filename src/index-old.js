var http = require('http'),
    os = require('os');

const echo_function = `shipIt = function() {
      const purchase_message =
          [{
            message: "kitten purchased",
            type: "INFO",
            routingKey: "sales",
            messageDetail: { appVersion: "` + process.env.SERVICE_TAGS + `"},
            timestamp : ` + Math.round(new Date().getTime()) + `
          }];

      const url = "http://` + process.env.ECHO_COLLECTOR + `/echo/messages";

      //update the timestamp
      purchase_message[0].timestamp = Math.round(new Date().getTime())


      $.ajax({
        url: url,
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(purchase_message),
        success: function( response ) {
          alert('Thanks!');
        },
        error: function() {
          alert( "error" );
        }
      });
    };
  `;

function handleRequest(request, response){
    const color = process.env.SERVICE_TAGS
    response.writeHeader(200, {"Content-Type": "text/html"})
    response.write("<html>");
    response.write("  <head>");
    response.write("    <meta charset='utf-8'>");
    response.write("    <meta http-equiv='X-UA-Compatible' content='IE=edge'>");
    response.write("    <meta name='viewport' content='width=device-width, initial-scale=1'>");
    response.write("    <title>Example</title>");
    response.write("      <style>");
    response.write("         body { padding-top: 20px; padding-bottom: 20px; }");
    response.write("         .header, .marketing, .footer { padding-right: 15px; padding-left: 15px; }");
    response.write("         .header { padding-bottom: 20px; border-bottom: 1px solid #e5e5e5; }");
    response.write("         .header h3 { margin-top: 0; margin-bottom: 0; line-height: 40px; }");
    response.write("         .footer { padding-top: 19px; color: #777; border-top: 1px solid #e5e5e5; }");
    response.write("         @media (min-width: 768px) { .container { max-width: 730px; } }");
    response.write("         .container-narrow > hr { margin: 30px 0; }");
    response.write("         .jumbotron { text-align: center; border-bottom: 1px solid #e5e5e5; }");
    response.write("         .jumbotron .btn { padding: 14px 24px; font-size: 21px; }");
    response.write("         .marketing { margin: 40px 0; }");
    response.write("         .marketing p + h4 { margin-top: 28px; }");
    response.write("         @media screen and (min-width: 768px) {");
    response.write("           .header, .marketing, .footer { padding-right: 0; padding-left: 0; }");
    response.write("           .header { margin-bottom: 30px; }");
    response.write("           .jumbotron { border-bottom: 0; }");
    response.write("         }");
    response.write("      </style>");
    response.write("    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' integrity='sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7' crossorigin='anonymous'>");
    response.write("    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css' integrity='sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r' crossorigin='anonymous'>");
    response.write("  </head>");



    response.write("  <body>");
    response.write("    <div class='jumbotron'>");
    response.write("      <h1>Buy Kittens!</h1>");
    response.write("      <div class='row'><img class='img-rounded' src='https://placekitten.com/g/200/200'/><p></p></div>");
      response.write("      <button onClick='shipIt()' class='btn btn-lg' style=background:" + color + " type='button'>I want one!</button>");
    response.write("    </div>");
    response.write("    <ul>");
    response.write("      <li>service name : " + process.env.SERVICE_NAME + "</li>");
    response.write("      <li>current color: " + color + "</li>");
    response.write("      <li>hostname : " + os.hostname() + "</li>");
    response.write("    </ul>");
    response.write("    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>");
    response.write("    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' integrity='sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS' crossorigin='anonymous'></script> ");
    response.write("    <script>");
    response.write(echo_function);
    response.write("    </script>");
    response.write("  </body>");
    response.end("</html>");
}

var server = http.createServer(handleRequest);

server.listen(3000, function(){
    console.log("Server listening on 3000");
});
