'use strict';

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const format = require('util').format;
const axios = require('axios');
const os = require('os');
const app = express();

app.set('views', './src/views')
app.set('view engine', 'hbs')

const env = process.env.NODE_ENV || 'development'
if (env === 'development') {
  const config = require('../webpack.config.js');
  const compiler = webpack(config);

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    watchOptions: {
      aggregateTimeout: 1000,
      poll: 1000
    },
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}
else {
  app.get('/static/index.js', (req, res) => {
    res.sendFile(path.join(__dirname, '../static/index.js'));
  });
}

app.get('/css/basket.css', (req, res) => {
  res.sendFile(path.join(__dirname, '../css/basket.css'));
});

const url = format("http://%s/echo/messages", process.env.ECHO_COLLECTOR);
app.get('*', (req, res) => {
  let env = {
    color: process.env.BUTTON_COLOR,
    name: process.env.SERVICE_NAME,
    tags: process.env.SERVICE_TAGS
  }
  let hostname = os.hostname()
  let message = [{
    host: hostname,
    message: "Page load",
    type: "INFO",
    routingKey: "pet-basket",
    messageDetail: env,
    timestamp: Math.round(new Date().getTime())
  }]
  axios.post(url, message)

  res.render('index', Object.assign(env, { collector: process.env.ECHO_COLLECTOR, hostname }))
});

app.listen(3000, '0.0.0.0', (err) => {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://0.0.0.0:3000');
});
