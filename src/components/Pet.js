import React from 'react';

class Pet extends React.Component {
  add(){
    this.props.addToBasket(this.props.type)
  }
  render(){
    let count = this.props.basket.filter((item) => item === this.props.type).length
    return (
      <div className="column">
        <h1>{this.props.type}s: {count}</h1>
        <img className="img-rounded" src={this.props.image} width="200px" />
        <button className="btn btn-lg" style={{ background: this.props.color, color: 'white' }} onClick={this.add.bind(this)}>Add to Basket</button>
      </div>
    );
  }
}

const { string, arrayOf, func } = React.PropTypes;
Pet.propTypes = {
  type: string,
  image: string,
  color: string,
  basket: arrayOf(string),
  addToBasket: func
};

Pet.defaultProps = {
  type: "dog",
  image: "http://i.giphy.com/IWon6VgzVwEnu.gif",
  color: "blue"
}

export default Pet
