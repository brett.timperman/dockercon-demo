import React from 'react';

class Info extends React.Component {
  add(){
    this.addToBasket(this.props.type)
  }
  render(){
    return (
      <ul>
        <li>service name: {this.props.name}</li>
        <li>service tags: {this.props.tags}</li>
        <li>hostname: {this.props.hostname}</li>
      </ul>
    );
  }
}

const { string } = React.PropTypes;
Info.propTypes = {
  name: string,
  tags: string,
  hostname: string
};

Info.defaultProps = {
  name: "root",
  tags: "",
  hostname: "hostname"
}

export default Info
