import { createActionAsync } from 'redux-act-async';
import { format } from 'util';
import axios from 'axios';

const url = format("http://%s/echo/messages", Config.collector);

const sendEchoMessage = (pet) => {
  let message = [{
    host: Config.hostname,
    message: "Add to basket",
    type: "SALES",
    routingKey: "pet-basket",
    messageDetail: { pet, color: Config.color, tags: Config.tags },
    timestamp: Math.round(new Date().getTime())
  }]
  console.log('sending echo message', message[0])
  return axios
    .post(url, message)
    .then(({data}) => data)
}

export const addToBasket = createActionAsync('Add to basket', sendEchoMessage);
