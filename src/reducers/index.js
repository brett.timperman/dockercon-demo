import { createReducer } from 'redux-act';
import { addToBasket } from '../actions';

const initialState = {
  meta: {
    name: Config.name,
    tags: Config.tags,
    hostname: Config.hostname
  },
  basket: []
};

const reducer = createReducer({
  [addToBasket.request]: (state, pet) => {
    return Object.assign({}, state, { basket: [...state.basket, pet] })
  },
  [addToBasket.ok]: (state, data) => {
    return state;
  },
  [addToBasket.error]: (state, error) => {
    console.log('error', error)
    return state;
  }
}, initialState)

export default reducer
