import React from 'react';
import { render } from 'react-dom';
import thunkMiddleware from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';
import { Demo } from './containers/Demo';

const store = createStore(
    reducers,
    compose(
      applyMiddleware(thunkMiddleware),
      window.devToolsExtension ? window.devToolsExtension() : (f) => f
    )
  );

class Main extends React.Component {
  render(){
    return (
      <Provider store={store}>
        <Demo />
      </Provider>
    );
  }
}

render(<Main />, document.getElementById('main'));
