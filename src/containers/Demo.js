import React from 'react';
import { connect } from 'react-redux';
import { addToBasket } from '../actions';
import Info from '../components/Info';
import Pet from '../components/Pet';

const mapStateToProps = (state) => {
  console.log('state', state)
  return { basket: state.basket, meta: state.meta };
}

const mapDispatchToProps = { addToBasket }

class Container extends React.Component {
  render(){
    let color = Config.color || "blue"
    return (
      <div style={{ display: 'flex', flexDirection: 'column', width: 320 }}>
        <Info name={this.props.meta.name} tags={this.props.meta.tags} hostname={this.props.meta.hostname} />
        <div class="row">
          <Pet type="dog" image="http://i.giphy.com/IWon6VgzVwEnu.gif" color={color}
            basket={this.props.basket} addToBasket={this.props.addToBasket} />
          <Pet type="cat" image="http://i.giphy.com/b3rbghhoMrhII.gif" color={color}
            basket={this.props.basket} addToBasket={this.props.addToBasket} />
        </div>
      </div>
    );
  }
}

const { arrayOf, string, shape, func } = React.PropTypes;
Container.propTypes = {
  basket: arrayOf(string),
  meta: shape({ name: string, tags: string, hostname: string }),
  addToBasket: func
};

export const Demo = connect(mapStateToProps, mapDispatchToProps)(Container);
